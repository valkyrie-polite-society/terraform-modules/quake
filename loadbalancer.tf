resource "google_compute_forwarding_rule" "quake" {
  name       = "quake"
  target     = google_compute_target_pool.quake.id
  port_range = "27500"
  ip_protocol = "UDP"
}

resource "google_compute_target_pool" "quake" {
  name = "quake"

  instances = [
    data.google_compute_instance.nomad_client.self_link
  ]
}
